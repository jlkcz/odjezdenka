from app.models import User
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField, ValidationError
from wtforms.validators import DataRequired, EqualTo, Email, NoneOf, Length

RESERVED_LINKNAMES = ['newlink', 'register', 'user', 'index', 'login', 'logout', 'mylinks', 'editlink', 'deleteconnection', 'moveleft', 'moveright', 'deletelink']


class LoginForm(FlaskForm):
    username = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Heslo', validators=[DataRequired()])
    remember_me = BooleanField('Pamatovat si přihlášení')
    submit = SubmitField('Přihlásit')


class RegistrationForm(FlaskForm):
    username = StringField('Login', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Heslo', validators=[DataRequired()])
    password_repeat = PasswordField('Heslo znovu', validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Zaregistrovat")

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Toto uživatelské jméno nelze použít')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Tento email nelze použít')


class ChangePasswordForm(FlaskForm):
    oldpass = PasswordField('Staré heslo', validators=[DataRequired()])
    newpass = PasswordField('Nové heslo', validators=[DataRequired()])
    newpass_repeat = PasswordField('Nové heslo znovu', validators=[DataRequired(), EqualTo("newpass")])
    submit = SubmitField("Změnit heslo")


class NewLinkForm(FlaskForm):
    linkname = StringField('Název odkazu', validators=[DataRequired(), Length(min=2, max=32), NoneOf(RESERVED_LINKNAMES)], description='Adresa, pod kterou budou vaše spojení, tedy https://o.llll.cz/<něco>')
    submit = SubmitField("Vytvořit nový link")


class NewConnectionForm(FlaskForm):
    departure = StringField('Odkud', validators=[DataRequired()])
    destination = StringField('Kam', validators=[DataRequired()])
    through = StringField('Přes zastávku (nepovinné')
    direct_only = BooleanField('Pouze přímá spojení, bez přestupů')
    lowtransport_only = BooleanField('Spojení pro kočárky')
    wheelchair_only = BooleanField('Jen bezbariérová spojení')
    link_id = HiddenField('Odkaz') #TODO: validate if user can add
    submit = SubmitField("Vytvořit nové spojení")

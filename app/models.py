import datetime
from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from sqlalchemy.orm import relationship


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    links = db.relationship("Link", backref='owner', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, newpass):
        self.password_hash = generate_password_hash(newpass)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    accessed = db.Column(db.Integer, default=0)
    last_access = db.Column(db.DateTime, default=datetime.datetime.now)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    connections = db.relationship("Connection", backref='link', lazy='dynamic', order_by="asc(Connection.position)", cascade='all, delete-orphan')

    def __repr__(self):
        return '<Link {}>'.format(self.name)

class Connection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    departure = db.Column(db.String(64))
    destination = db.Column(db.String(64))
    through = db.Column(db.String(64))
    direct_only = db.Column(db.Boolean)
    position = db.Column(db.Integer)
    wheelchair_only = db.Column(db.Boolean)
    lowtransport_only = db.Column(db.Boolean)
    link_id = db.Column(db.Integer, db.ForeignKey('link.id'))

    def __repr__(self):
        return '(Connection: {}-{})'.format(self.departure, self.destination)


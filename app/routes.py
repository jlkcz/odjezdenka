import datetime
from app import app, render_template, db, dpp
from app.forms import LoginForm, RegistrationForm, ChangePasswordForm, NewLinkForm, NewConnectionForm
from app.models import User, Link, Connection
from flask import redirect, url_for, flash, request
from flask_login import logout_user, login_user, login_required, current_user
from werkzeug.urls import url_parse



@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Neplatné uživatelské jméno nebo heslo')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    flash('Úspěšně odhlášen')
    return redirect(url_for('login'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Registrace úspěšná, nyní se prosím přihlašte')
        return redirect('login')
    return render_template('register.html', form=form)

@app.route('/user', methods=['GET', 'POST'])
@login_required
def user():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if not current_user.check_password(form.oldpass.data):
            flash('Staré heslo nesedí')
            return(redirect('user'))
        current_user.set_password(form.newpass.data)
        db.session.commit()
        flash('Heslo úspěšně změněno')
    return render_template('user.html', form=form)

@app.route('/mylinks')
@login_required
def mylinks():
    return render_template("mylinks.html", links=current_user.links)

@app.route('/newlink', methods=['GET', 'POST'])
@login_required
def newlink():
    form = NewLinkForm()
    if form.validate_on_submit():
        link = Link(name=form.linkname.data, user_id=current_user.id)
        db.session.add(link)
        db.session.commit()
        flash('Link úspěšně vytvořen')
        return redirect(url_for('editlink', linkname=form.linkname.data))
    return render_template('newlink.html', form=form)

@app.route('/editlink/<linkname>', methods=['GET', 'POST'])
@login_required
def editlink(linkname):
    link = Link.query.filter_by(name=linkname, user_id=current_user.id).first()
    #checks if link exists and the user is the owner
    if link is None:
        flash('Odkaz nenalezen nebo není dostupný', category="error")
        return redirect(url_for('mylinks'))

    form = NewConnectionForm()
    if form.validate_on_submit():
        newconn = Connection(departure=form.departure.data, destination=form.destination.data, through=form.through.data, direct_only=form.direct_only.data, link_id=link.id, wheelchair_only=form.wheelchair_only.data, lowtransport_only=form.lowtransport_only.data)
        #calculation position
        if link.connections.count() == 0:
            newconn.position = 1
        else:
            newconn.position = link.connections[-1].position + 1
        db.session.add(newconn)
        db.session.commit()
        flash("Nové spojení úspěšně přidáno", category="info")
        return redirect(url_for('editlink', linkname=link.name)) # prevents refreshposts
    form.link_id = link.id
    return render_template('editlink.html', link=link, form=form)

@app.route("/deleteconnection/<id>")
@login_required
def deleteconnection(id):
    connection = Connection.query.filter_by(id=id).first()
    if connection is None or connection.link.user_id != current_user.id:
        flash("Spojení neexistuje, nebo je nedostupné", "error")
        return redirect(url_for("index"))

    db.session.delete(connection)
    db.session.commit()
    flash("Spojení smazáno")
    return redirect(url_for("editlink", linkname=connection.link.name))

@app.route("/moveleft/<id>")
@login_required
def moveleft(id):
    this_connection = Connection.query.filter_by(id=id).first_or_404()
    if this_connection.link.user_id != current_user.id:
        flash("Spojení neexistuje, nebo je nedostupné", "error")
        return redirect(url_for('index'))

    pushed_connection = Connection.query.filter_by(link_id=this_connection.link_id, position=this_connection.position-1).first()
    this_connection.position -= 1
    if pushed_connection is not None:
        pushed_connection.position += 1
    db.session.commit()
    return redirect(url_for("editlink", linkname=this_connection.link.name))

@app.route("/moveright/<id>")
@login_required
def moveright(id):
    this_connection = Connection.query.filter_by(id=id).first_or_404()
    if this_connection.link.user_id != current_user.id:
        flash("Spojení neexistuje, nebo je nedostupné", "error")
        return redirect(url_for('index'))

    pushed_connection = Connection.query.filter_by(link_id=this_connection.link_id, position=this_connection.position+1).first()
    this_connection.position += 1
    if pushed_connection is not None:
        pushed_connection.position -= 1
    db.session.commit()
    return redirect(url_for("editlink", linkname=this_connection.link.name))




@app.route("/deletelink/<linkname>")
@login_required
def deletelink(linkname):
    link = Link.query.filter_by(name=linkname, user_id=current_user.id).first_or_404()
    db.session.delete(link)
    db.session.commit()
    flash("Odkaz {} byl smazán".format(link.name))
    return redirect(url_for("mylinks"))

@app.route('/<linkname>')
def link(linkname):
    link = Link.query.filter_by(name=linkname).first_or_404()
    link.accessed += 1
    link.last_access = datetime.datetime.now()
    routes = []
    for connection in link.connections:
        try:
            newconn = dpp.DPP(connection.departure, connection.destination, direct_only=connection.direct_only, wheelchair_only=connection.wheelchair_only, lowtransport_only=connection.lowtransport_only)
            newconn.find_connection()
        except dpp.NoResults:
            routes.append((connection, None))
            continue
        else:
            routes.append((connection, newconn)
                          )
    db.session.commit()
    return render_template("link.html", link=link, routes=routes)

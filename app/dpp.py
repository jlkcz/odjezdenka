#!/usr/bin/env python3

import re
import sys
import requests
import datetime
from bs4 import BeautifulSoup


class NoResults(Exception):
    pass


class Connection:
    def __init__(self, from_st, to_st):
        self.timeline = []
        self.trip_from = from_st
        self.trip_to = to_st
        self.complete_time = ''

    def add_foot(self, text):
        self.timeline.append(ByFoot(text))

    def add_dpp(self, from_station, from_time, to_station, to_time, line):
        self.timeline.append(
            ByDPP(from_station, from_time, to_station, to_time, line)
        )


class ByFoot:
    def __init__(self, text):
        self.text = text

    def __repr__(self):
        return "<ByFoot {}>".format(self.text)


class ByDPP:
    def __init__(self, from_station, from_time, to_station, to_time, line):
        self.from_station = from_station
        self.from_time = from_time
        self.to_station = to_station
        self.to_time = to_time
        self.line = line

    def __repr__(self):
        return "<ByDPP: Line {} from {} ({}) to {} ({})>".format(self.line, self.from_station, self.from_time, self.to_station, self.to_time)


class DPP:
    def __init__(self, from_st, to_st, direct_only=False, lowtransport_only=False, wheelchair_only=False):
        self.from_st = from_st
        self.to_st = to_st
        self.direct_only = direct_only
        self.lowtransport_only = lowtransport_only
        self.wheelchair_only = wheelchair_only
        self.date = str(datetime.datetime.now().strftime('%d.%m.%Y'))
        self.time = str(datetime.datetime.now().strftime('%H:%M'))

        self.connections = []

    def get_form_data(self):
        self.form = requests.get('http://spojeni.dpp.cz')
        self.soup = BeautifulSoup(self.form.text, 'html.parser')
        self.formdata = {}
        for inp in self.soup.select("form#ctlForm input[type=hidden]"):
            try:
                self.formdata[inp['name']] = inp['value']
            except KeyError:
                pass

    def add_request_data(self):
        self.formdata['txtDate'] = self.date
        self.formdata['txtTime'] = self.time
        self.formdata['txtFrom'] = self.from_st
        self.formdata['txtTo'] = self.to_st
        self.formdata['ctlFrom$txtObject'] = self.from_st
        self.formdata['ctlTo$txtObject'] = self.to_st
        self.formdata['Direction'] = 'optDeparture'
        self.formdata['ctlVia$txtObject'] = ''
        self.formdata['ctlVia$txtVirtListItemCode'] = ''
        self.formdata['ctlVia$txtSearchMode'] = 0
        self.formdata['ctlTo$txtSearchMode'] = '0'
        self.formdata['cmdSearch'] = ''
        if self.direct_only:
            self.formdata['optChangesDirect'] = 'on'
        if self.lowtransport_only:
            self.formdata['chkLowDeckConnTr'] = 'on'
        if self.wheelchair_only:
            self.formdata['chkLowDeckConnection'] = 'on'

    def make_request(self):
        result = requests.post('http://spojeni.dpp.cz', data=self.formdata)
        self.resultsoup = BeautifulSoup(result.text, 'html.parser')

    def parse_request(self):
        alltickets = self.resultsoup.select(".Box-ticket:not(.Box--shadow)")
        if not alltickets:
            raise NoResults
        for ticket in alltickets:
            this_conn = Connection(self.from_st, self.to_st)
            this_conn.complete_time = ticket.select('.LineTrack-tripTime')[0].string
            #changes = jednotlivé části spojení
            changes = ticket.select('table.LineTrack-connections > tbody > tr')
            for change in changes:
                #přesun pěšky
                if len(change.select('.LineTrack-transferDotsWrap')) != 0:
                    if not len(change.select('.LineTrack-delay')) != 0:
                        this_conn.add_foot(change.select('td.LineTrack-transferDotsWrap > div.LineTrack-transfer > span.u-xs-hidden > i')[0].string)
                    continue #přesun pěšky nemá další info
                  #inicializace polí s výsledky
                lines = []
                stops = []
                times = []
                #není to pěšky, takže je to linka.
                for line in change.select('.LineTrack-tripLine > a'):
                     lines.append((line.select('div > strong > span')[0].string))
                 #samotné stanice a časy
                for li in change.select('ul.Box-lineTrack > li'):
                     if 'class' in li.attrs and 'LineTrack-stopStartPoint' in li.attrs['class']:
                         times.append(li.select('span.u-inlineBlock > strong')[0].string)
                         times.append(li.select('span.u-inlineBlock > strong')[1].string)
                         stops.append(li.select('strong.u-decorationUnderline')[0].string)
                         stops.append(li.select('strong.u-decorationUnderline')[0].string)
                     else:
                        try:
                             stops.append(li.select('strong.u-decorationUnderline')[0].string)
                             times.append(li.select('div.LineTrack-stopTime > span > strong')[0].string)
                        except IndexError as e: #this is a hack, but apparently there is something rotten in the web which complicates detecting errorneous connections
                            raise NoResults


                for line in lines:
                    this_conn.add_dpp(
                        stops.pop(0), times.pop(0), stops.pop(0), times.pop(0), line
                    )
            self.connections.append(this_conn)

    def find_connection(self):
        self.get_form_data()
        self.add_request_data()
        self.make_request()
        self.parse_request()
        return self.connections



# #ticket = jedna celá jízda
# for ticket in resultsoup.select(".Box-ticket:not(.Box--shadow)"):
#     print("Spojení z Muzea na Spořilov:")
#     print(ticket.select('.LineTrack-tripTime')[0].string)
#     #changes = jednotlivé části spojení
#     changes = ticket.select('table.LineTrack-connections > tbody > tr')
#     for change in changes:
#         #přesun pěšky
#         if len(change.select('.LineTrack-transferDotsWrap')) != 0:
#             print(change.select('td.LineTrack-transferDotsWrap > div.LineTrack-transfer > span.u-xs-hidden > i')[0].string)
#             print("---")
#             continue #přesun pěšky nemá další info
#         #inicializace polí s výsledky
#         lines = []
#         stops = []
#         times = []
#         #není to pěšky, takže je to linka.
#         for line in change.select('.LineTrack-tripLine > a'):
#             lines.append((line.select('div > strong > span')[0].string))
#         #samotné stanice a časy
#         for li in change.select('ul.Box-lineTrack > li'):
#             if 'class' in li.attrs and 'LineTrack-stopStartPoint' in li.attrs['class']:
#
#                 times.append(li.select('span.u-inlineBlock > strong')[0].string)
#                 times.append(li.select('span.u-inlineBlock > strong')[1].string)
#                 stops.append(li.select('strong.u-decorationUnderline')[0].string)
#                 stops.append(li.select('strong.u-decorationUnderline')[0].string)
#             else:
#                 stops.append(li.select('strong.u-decorationUnderline')[0].string)
#                 times.append(li.select('div.LineTrack-stopTime > span > strong')[0].string)
#         print('---')
#         print(lines)
#         print(stops)
#         print(times)
#         print('========================')

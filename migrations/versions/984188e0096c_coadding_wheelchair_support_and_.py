"""coadding wheelchair support and position to connection

Revision ID: 984188e0096c
Revises: 734ea40412ef
Create Date: 2020-04-23 11:48:01.051944

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '984188e0096c'
down_revision = '734ea40412ef'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('connection', sa.Column('position', sa.Integer(), nullable=True))
    op.add_column('connection', sa.Column('wheelchair_only', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('connection', 'wheelchair_only')
    op.drop_column('connection', 'position')
    # ### end Alembic commands ###

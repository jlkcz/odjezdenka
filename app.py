#!/usr/bin/env python3
from os import path, walk
from app import app

if __name__ == '__main__':
    app.run(debug=True)
